# craft-exec

You can supply the command in the multi-environment config.

```
return array(
  '*' => array(
    'execCommand' => 'sudo /usr/bin/find /var/cache/nginx -type f -delete && curl http://notify.service'
  ),
  'local' => array(
    'execCommand' => ''
  ),
);
```
