<?php
namespace Craft;

class ExecPlugin extends BasePlugin
{
    public function init()
    {
        craft()->entries->onSaveEntry = function(Event $event) {
            $this->runExec();
        };
        craft()->categories->onSaveCategory = function(Event $event) {
            $this->runExec();
        };
        craft()->globals->onSaveGlobalContent = function(Event $event) {
            $this->runExec();
        };
    }

    function getName()
    {
        return Craft::t('Exec');
    }

    function getVersion()
    {
        return '0.1';
    }

    function getDeveloper()
    {
        return 'EverySquare';
    }

    function getDeveloperUrl()
    {
        return 'http://everysquare.ca';
    }

    public function runExec()
    {
        $command = craft()->config->get('execCommand');
        exec($command);
    }
}
